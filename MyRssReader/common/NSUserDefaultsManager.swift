//
//  NSUserDefaultsManager.swift
//  MyRssReader
//
//  Created by usr0301057 on 2016/10/31.
//  Copyright © 2016年 usr0301057. All rights reserved.
//

import Foundation

class NSUserDefaultsManager {
    
    private static let userDefault : NSUserDefaults = NSUserDefaults.standardUserDefaults()
    
    static var isDidTutorial: Bool {
        set {
            userDefault.setBool(newValue, forKey: "alreadyDisplayTutorial")
            userDefault.synchronize()
        }
        
        get {
            return userDefault.boolForKey("alreadyDisplayTutorial")
        }
    }
    
    static func getUserID() -> String {
        var userID : String = ""
        if let obj = userDefault.objectForKey(NSUserDefaultKeyEnum.USER_ID.getKey()){
            userID = obj as! String
        }
        return userID
    }
    
    static func setObjectAndSynchronize(value: AnyObject?, forKey defaultName: String){
        userDefault.setObject(value, forKey: defaultName)
        userDefault.synchronize()
    }
    
    static func setIntegerAndSynchronize(value: Int, forKey defaultName: String){
        userDefault.setInteger(value, forKey: defaultName)
        userDefault.synchronize()
    }
}
