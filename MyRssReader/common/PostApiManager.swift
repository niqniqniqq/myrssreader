//
//  PostApiManager.swift
//  MyRssReader
//
//  Created by usr0301057 on 2016/10/27.
//  Copyright © 2016年 usr0301057. All rights reserved.
//

import Foundation
import UIKit

class PostApiManager {
    
    var uuid : String?
    var deviceToken : String?
    var osVersion : String?
    
    init(uuid : String, deviceToken : String){
        self.uuid = uuid
        self.deviceToken = deviceToken
        self.osVersion = UIDevice.currentDevice().systemVersion
    }
    
    func createPushSettingJsonData() -> String {
        
        /**
         "version": "1.0",
         "date": "YYYY-MM-DD HH:MM:SS",
         "app": {
         "build": "XXXX",
         "version": "X.Y.Z",
         "platform": "XXXX",
         "ios": "X.Y.Z",
         "android": "X.Y.Z",
         "deviceId": "XXXX",
         "deviceToken: "XXXX"
         },
         "data": {
         "pushConfiｇ": {
         "isMorningPush" : "1"
         "isMorningPush" : "1"
         "isMorningPush" : "1"
         "isMorningPush" : "1"
         },
         }
         */
        
        //プッシュ設定値読み込み
        let userDefault : NSUserDefaults = NSUserDefaults.standardUserDefaults()
        var isMorningPush : Int = 1
        var isNoonPush : Int = 1
        var isEveningPush : Int = 1
        var isNightPush : Int = 1
        if let pushSettingObj = userDefault.objectForKey(NSUserDefaultKeyEnum.IS_MORNING_PUSH.getKey()){
            isMorningPush = pushSettingObj as! Int
        }
        if let pushSettingObj = userDefault.objectForKey(NSUserDefaultKeyEnum.IS_NOON_PUSH.getKey()){
            isNoonPush = pushSettingObj as! Int
        }
        if let pushSettingObj = userDefault.objectForKey(NSUserDefaultKeyEnum.IS_EVENING_PUSH.getKey()){
            isEveningPush = pushSettingObj as! Int
        }
        if let pushSettingObj = userDefault.objectForKey(NSUserDefaultKeyEnum.IS_NIGHT_PUSH.getKey()){
            isNightPush = pushSettingObj as! Int
        }
        
        //json組み立て
        let appArray : [String : String] = [
            "build": NSBundle.mainBundle().objectForInfoDictionaryKey("CFBundleVersion") as! String,
            "version": NSBundle.mainBundle().objectForInfoDictionaryKey("CFBundleShortVersionString") as! String,
            "platform": "ios",
            "ios": osVersion!,
            "deviceId": uuid!,
            "deviceToken": deviceToken!,
            ]
        
        let  pushConfig : [String : String] = [
            "isMorningPush" : isMorningPush.description,
            "isNoonPush" : isNoonPush.description,
            "isEveningPush" : isEveningPush.description,
            "isNightPush" : isNightPush.description,
            ]
        
        let data : [String: AnyObject] = [
            "pushConfig" : pushConfig
        ]
        
        let apiArray : [String: AnyObject] = [
            "version": "1.0",
            "date": DataUtil.getNowDateByFormat(DateFormatEnum.ymdhmsFormat1),
            "app" : appArray,
            "data" : data
        ]
        
        var urlEncodeJson : String = ""
        do{
            let jsonData = try NSJSONSerialization.dataWithJSONObject(apiArray, options: NSJSONWritingOptions(rawValue: 0))
            urlEncodeJson = (NSString(data: jsonData, encoding: NSUTF8StringEncoding)?.description)!
        } catch {
            print("json convert error")
        }
        
        return urlEncodeJson
    }
    
    func postApi(postJson : String) -> Bool {
        var result : Bool = true
        let session = NSURLSession.sharedSession()
        if let nsUrl = NSURL.init(string: ApiUrlEnum.ENDPOINT_URL.getUrl()){
            let request = NSMutableURLRequest.init(URL: nsUrl)
            request.HTTPMethod = "POST"
            request.HTTPBody = postJson.dataUsingEncoding(NSUTF8StringEncoding)
            let task = session.dataTaskWithRequest(request, completionHandler: { (nsData : NSData?, nsUrlResponse : NSURLResponse?, nsError : NSError?) in
                //エラー処理
                if let error = nsError{
                    print("#####post api error ######:" + error.description)
                    result = false
                    return
                }
                
                //200以外のステータスコード検知
                if let httpResponse = nsUrlResponse as? NSHTTPURLResponse{
                    if 200 != httpResponse.statusCode{
                        print("#####:post api response#####: " + httpResponse.statusCode.description)
                        result = false
                        return
                    }
                }
                
                //userIDを保存
                do {
                    let nsDic = try NSJSONSerialization.JSONObjectWithData(nsData!, options: NSJSONReadingOptions.MutableContainers) as! NSDictionary
                    let resutValue = nsDic.objectForKey("result") as! NSDictionary
                    let dataArray = resutValue.objectForKey("data1") as! NSDictionary
                    let userID = dataArray.objectForKey("user_id")?.integerValue.description
                    let userDefault : NSUserDefaults = NSUserDefaults.standardUserDefaults()
                    userDefault.setValue(userID, forKey: NSUserDefaultKeyEnum.USER_ID.getKey())
                    userDefault.synchronize()
                } catch  {
                    // エラー処理
                }
            })
            task.resume()
        }
        return result
    }
    
}
