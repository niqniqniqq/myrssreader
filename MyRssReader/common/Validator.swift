//
//  Validator.swift
//  MyApiTestApp
//
//  Created by usr0301057 on 2016/08/24.
//  Copyright © 2016年 usr0301057. All rights reserved.
//

import Foundation

protocol Validator {
    func validateURL(urlString: String) -> Bool
}

extension Validator {
    func validateURL(urlString: String) -> Bool {
        var result = false
        let types: NSTextCheckingType = .Link
        let detector = try? NSDataDetector(types: types.rawValue)
        guard let detect = detector else {
            return result
        }
        let matches = detect.matchesInString(urlString, options: .ReportCompletion, range: NSMakeRange(0, urlString.characters.count))
        for match in matches {
            if let url = match.URL {
                print("######validation OK : url \(url)#########")
                result = true
            }
        }
        return result
    }
}

//NSTextCheckingTypeでチェックしたい項目が増えたら同様に拡張していく