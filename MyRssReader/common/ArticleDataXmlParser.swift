//
//  GetArticleListFromXml.swift
//  MyRssReader
//
//  Created by usr0301057 on 2016/08/29.
//  Copyright © 2016年 usr0301057. All rights reserved.
//

import Foundation

class ArticleDataXmlParser : NSObject, NSXMLParserDelegate {
    //入力xml
    var inputXml : NSData?
    //xml処理変数
    var xmlitemArray = [[String]]()
    //xml処理変数
    var nowParseTag : String?
    //xml処理変数
    var itemIndex : Int = 0
    
    //要素毎のパース回数(英数字とマルチバイト文字が含まれる場合、自動的に分割されるため)
    var elementParseCnt : Int = 0
    
    init (input : NSData!){
        inputXml = input
    }
    
    //data取得処理
    func getArticleData() -> [[String]]{
        //初期化
        xmlitemArray.removeAll()
        
        //取得xmlデータ処理
        if let xmlString = NSString.init(data: inputXml!, encoding: NSUTF8StringEncoding) as? String {
            //xml解析
            print(xmlString)
            let parser = NSXMLParser(data: inputXml!)
            parser.delegate = self
            parser.parse()
        }
        return xmlitemArray
    }
    
    // XML解析開始時に実行されるメソッド
    func parserDidStartDocument(parser: NSXMLParser) {
        itemIndex = 0;
    }
    
    // 解析中に要素の開始タグがあったときに実行されるメソッド
    func parser(parser: NSXMLParser, didStartElement elementName: String, namespaceURI: String?, qualifiedName qName: String?, attributes attributeDict: [String : String]) {
        nowParseTag = elementName
        elementParseCnt = 0
        if "item" == elementName{
            xmlitemArray.append([String]())
        }
    }
    
    // 開始タグと終了タグでくくられたデータがあったときに実行されるメソッド
    func parser(parser: NSXMLParser, foundCharacters string: String) {
        //前後スペース、前後改行コード削除
        let string = string.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet())
        if "id" == nowParseTag{
            xmlitemArray[itemIndex].insert(string, atIndex: 0)
        } else if "title" == nowParseTag{
            var title = string
            if 0 < elementParseCnt{
                title = xmlitemArray[itemIndex][1] + title
            }
            xmlitemArray[itemIndex].insert(title, atIndex: 1)
        } else if "link" == nowParseTag{
            xmlitemArray[itemIndex].insert(string, atIndex: 2)
        } else if "category" == nowParseTag {
            var category = string
            if 0 < elementParseCnt{
                category = xmlitemArray[itemIndex][3] + category
            }
            xmlitemArray[itemIndex].insert(category, atIndex: 3)
        } else if "pubDate" == nowParseTag {
            xmlitemArray[itemIndex].insert(string, atIndex: 4)
        } else if "url" == nowParseTag{
            xmlitemArray[itemIndex].insert(string, atIndex: 5)
        }
        elementParseCnt += 1
    }
    
    // 解析中に要素の終了タグがあったときに実行されるメソッド
    func parser(parser: NSXMLParser, didEndElement elementName: String, namespaceURI: String?, qualifiedName qName: String?) {
        nowParseTag = ""
        if "item" == elementName {
            itemIndex += 1
        }
    }
    
    // XML解析終了時に実行されるメソッド
    func parserDidEndDocument(parser: NSXMLParser) {
    }
    
    // 解析中にエラーが発生した時に実行されるメソッド
    func parser(parser: NSXMLParser, parseErrorOccurred parseError: NSError) {
        print("xml parse error :" + parseError.localizedDescription)
    }
}
