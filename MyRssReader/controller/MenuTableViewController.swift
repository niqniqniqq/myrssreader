//
//  MenuTableViewController.swift
//  MyRssReaderUITest
//
//  Created by usr0301057 on 2016/09/07.
//  Copyright © 2016年 usr0301057. All rights reserved.
//

import UIKit
import MessageUI
import TabPageViewController

class MenuTableViewController: UITableViewController, MFMailComposeViewControllerDelegate {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
        
        //表示位置調節
        let statusHeight = UIApplication.sharedApplication().statusBarFrame.height
        self.tableView!.contentInset.top = statusHeight
        
        //余分な罫線を抑制
        self.tableView.tableFooterView = UIView()
        self.tableView.scrollEnabled = false
        
        //nav bar tool bar 初期設定
        if let nav = self.navigationController {
            nav.setNavigationBarHidden(true, animated: false)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //cell別タップ処理
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let cell = tableView.cellForRowAtIndexPath(indexPath)
        if let identifier = cell?.reuseIdentifier{
            if CellIdentifierEnum.SUPPORT.getIdentifier() == identifier {
                createSupportMailViewController()
            } else if CellIdentifierEnum.WRITE_REVIEW.getIdentifier() == identifier {
                let itunesURL:String = ApiUrlEnum.REVIEW_URL.getUrl()
                let url = NSURL(string:itunesURL)
                let app:UIApplication = UIApplication.sharedApplication()
                app.openURL(url!)
            }
        }
    }
    
    //サポートメール送信用コントローラーを作成
    //メールコントローラーxcodeシミュレータバグ有り
    func createSupportMailViewController(){
        //メールを送信できるかチェック
        if MFMailComposeViewController.canSendMail()==false {
            print("Email Send Failed")
            return
        }
        
        let mailViewController = MFMailComposeViewController()
        
        mailViewController.mailComposeDelegate = self
        mailViewController.setSubject(SupportMailEnum.TITLE.rawValue)
        mailViewController.setToRecipients([SupportMailEnum.TO_MAIL.rawValue]) //Toアドレスの表示
        mailViewController.setCcRecipients([String]())//cc
        mailViewController.setBccRecipients([String]())//bcc
        //本文作成
        let userID : String = NSUserDefaultsManager.getUserID()
        let description = SupportMailEnum.DISCRIPTION.rawValue.stringByReplacingOccurrencesOfString("(##USER_ID##)", withString: userID)
        mailViewController.setMessageBody(description, isHTML: false)
        
        self.presentViewController(mailViewController, animated: true, completion: nil)
    }
    
    //メールコントローラ制御プロトコル実装
    func mailComposeController(controller: MFMailComposeViewController, didFinishWithResult result: MFMailComposeResult, error: NSError?) {
        
        switch result.rawValue {
        case MFMailComposeResult.Cancelled.rawValue:
            print("Email Send Cancelled")
        case MFMailComposeResult.Saved.rawValue:
            print("Email Saved as a Draft")
        case MFMailComposeResult.Sent.rawValue:
            print("Email Sent Successfully")
        case MFMailComposeResult.Failed.rawValue:
            print("Email Send Failed")
        default:
            break
        }
        
        //コントローラーを破棄して一つ前に戻る
        self.dismissViewControllerAnimated(true, completion: nil)
    }
}
