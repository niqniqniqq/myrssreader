//
//  SharaPageTableViewController.swift
//  MyRssReaderUITest
//
//  Created by usr0301057 on 2016/09/12.
//  Copyright © 2016年 usr0301057. All rights reserved.
//

import UIKit
import Social
import MessageUI

class SharaPageTableViewController: UITableViewController, MFMailComposeViewControllerDelegate {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.delegate = self
        self.tableView.dataSource = self
        //nav bar設定
        self.title = PageTitleEnum.SHARE_PGAGE.getPageTitle()
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //cell別タップ処理
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let cell = tableView.cellForRowAtIndexPath(indexPath)
        if let identifier = cell?.reuseIdentifier{
            if CellIdentifierEnum.POST_TWITTER.getIdentifier() == identifier {
                postToTwitter()
            } else if CellIdentifierEnum.SEND_LINE.getIdentifier() == identifier {
                sendLine()
            } else if CellIdentifierEnum.SEND_MAIL.getIdentifier() == identifier {
                createSendMailController()
            }
        }
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
    }
    
    func postToTwitter() {
        // SLComposeViewControllerのインスタンス化.
        let composeViewCotroller = SLComposeViewController(forServiceType: SLServiceTypeTwitter)
        
        // 投稿するテキストを指定.
        composeViewCotroller.setInitialText(SharePageEnum.SHARE_TWITTER_DESCRIPTION.rawValue)
        
        // myComposeViewの画面遷移.
        self.presentViewController(composeViewCotroller, animated: true, completion: nil)
    }
    
    func sendLine() {
        let encodeMessage: String = SharePageEnum.SHARE_LINE_DESCRIPTION.rawValue.stringByAddingPercentEncodingWithAllowedCharacters(NSCharacterSet.URLQueryAllowedCharacterSet())!
        let messageURL: NSURL! = NSURL( string: "line://msg/text/" + encodeMessage )
        if (UIApplication.sharedApplication().canOpenURL(messageURL)) {
            UIApplication.sharedApplication().openURL( messageURL )
        } else {
            let alert: UIAlertController = UIAlertController(title: "LINEがインストールされていません。\nインストールしてからお試し下さい。", message: "", preferredStyle:  UIAlertControllerStyle.Alert)
            let okAction: UIAlertAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler:{
                (action: UIAlertAction!) -> Void in
            })
            
            alert.addAction(okAction)
            presentViewController(alert, animated: true, completion: nil)
        }
    }
    
    func createSendMailController(){
        //メールを送信できるかチェック
        if MFMailComposeViewController.canSendMail()==false {
            print("Email Send Failed")
            return
        }
        
        let mailViewController = MFMailComposeViewController()
        
        mailViewController.mailComposeDelegate = self
        mailViewController.setSubject(SharePageEnum.SHARE_MAIL_TITLE.rawValue)
        mailViewController.setMessageBody(SharePageEnum.SHARE_MAIL_DISCRIPTION.rawValue, isHTML: false)
        
        self.presentViewController(mailViewController, animated: true, completion: nil)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    //メールコントローラ制御プロトコル実装
    func mailComposeController(controller: MFMailComposeViewController, didFinishWithResult result: MFMailComposeResult, error: NSError?) {
        
        switch result.rawValue {
        case MFMailComposeResult.Cancelled.rawValue:
            print("Email Send Cancelled")
        case MFMailComposeResult.Saved.rawValue:
            print("Email Saved as a Draft")
        case MFMailComposeResult.Sent.rawValue:
            print("Email Sent Successfully")
        case MFMailComposeResult.Failed.rawValue:
            print("Email Send Failed")
        default:
            break
        }
        
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    override func viewWillAppear(animated: Bool) {
        if let nav = self.navigationController {
            nav.setNavigationBarHidden(false, animated: animated)
            nav.setToolbarHidden(true, animated: true)
        }
        super.viewWillAppear(animated)
    }
    
    override func viewWillDisappear(animated: Bool) {
        if let nav = self.navigationController {
            nav.setNavigationBarHidden(true, animated: animated)
            nav.setToolbarHidden(false, animated: true)
        }
        super.viewWillDisappear(animated)
    }

}
