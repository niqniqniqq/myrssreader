//
//  ArtistGroupSegmentViewController.swift
//  MyRssReaderUITest
//
//  Created by usr0301057 on 2016/09/07.
//  Copyright © 2016年 usr0301057. All rights reserved.
//

import UIKit

class ArtistGroupSegmentViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, CustomTableViewDelegate {
    
    //タブ設定数上限値
    let MAX_TAG_CNT : Int = 20
    
    //ターゲットEnum
    var targetEnum : ArtistGroupListEnum = ArtistGroupListEnum.GROUP_LIST
    
    //画面読み込み時点で設定されてるタブを保持する配列
    var tmpStoreArtistGroup : [ArtistGroupData]?
    
    @IBOutlet weak var artistGroupTableView: UITableView!

    @IBOutlet weak var artistGroupSegmentControll: UISegmentedControl!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        artistGroupTableView.delegate = self
        artistGroupTableView.dataSource = self
        
        //title設定
        self.title = PageTitleEnum.ADD_TAB_PAGE.getPageTitle()

        // Do any additional setup after loading the view.
        
        //表示位置調節
        let statusHeight = UIApplication.sharedApplication().statusBarFrame.height
        artistGroupTableView.contentInset.top = statusHeight
        
        //一覧取得
        ArtistGroupDataManager.sharedInstance.delegate = self
        ArtistGroupDataManager.sharedInstance.getArtistFromApi(targetEnum)
        
        //タブ追加検知用に、保存済みタブ一覧を保持
        tmpStoreArtistGroup = ArtistGroupDataManager.sharedInstance.storedArtistGroupArray
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func tabSegmentControll(sender: AnyObject) {
        //セグメント番号で条件分岐させる
        switch sender.selectedSegmentIndex {
        case 0:
            targetEnum = ArtistGroupListEnum.GROUP_LIST
        case 1:
            targetEnum = ArtistGroupListEnum.SUB_GROUP_LIST
        default:
            targetEnum = ArtistGroupListEnum.GROUP_LIST
        }
        
        //一度空データで描画
        ArtistGroupDataManager.sharedInstance.artistGroupArray.removeAll(keepCapacity: false)
        artistGroupTableView.reloadData()
        
        //一覧取得
        ArtistGroupDataManager.sharedInstance.getArtistFromApi(targetEnum)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return ArtistGroupDataManager.sharedInstance.artistGroupArray.count
    }
    
    //cell情報設定
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        //data取得
        let artistGroupData = ArtistGroupDataManager.sharedInstance.artistGroupArray[indexPath.row]
        //cell取得
        let cell = tableView.dequeueReusableCellWithIdentifier(CellIdentifierEnum.ARTIST_GROUP_CELL.getIdentifier(), forIndexPath: indexPath) as? ArticleGroupListTableViewCell
        cell?.nameLabel.text = artistGroupData.name
        //cellが選択されているか
        if artistGroupData.isAddTab {
            cell?.accessoryType = UITableViewCellAccessoryType.Checkmark
        } else {
            cell?.accessoryType = UITableViewCellAccessoryType.None
        }
        
        return cell!
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        //タブ情報取得
        let artistGroupData = ArtistGroupDataManager.sharedInstance.artistGroupArray[indexPath.row]
        
        //タブ設定数上限でないかチェック
        if !artistGroupData.isAddTab && MAX_TAG_CNT < ArtistGroupDataManager.sharedInstance.storedArtistGroupArray.count {
            let alert: UIAlertController = UIAlertController(title: "アーティスト・グループの設定上限は" + MAX_TAG_CNT.description + "です", message: "", preferredStyle:  UIAlertControllerStyle.Alert)
            let okAction: UIAlertAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler:{
                (action: UIAlertAction!) -> Void in
            })
            alert.addAction(okAction)
            presentViewController(alert, animated: true, completion: nil)
            //選択状態を元に戻す
            tableView.deselectRowAtIndexPath(indexPath, animated: true)
            return
        }
        
        //cell更新
        artistGroupData.isAddTab = !artistGroupData.isAddTab
        tableView.reloadRowsAtIndexPaths([indexPath], withRowAnimation: UITableViewRowAnimation.Fade)
        
        //userdefaultへ保持
        var isMatch : Bool  = false
        for storedData in ArtistGroupDataManager.sharedInstance.storedArtistGroupArray{
            if storedData.id == artistGroupData.id &&
                storedData.isGroup == artistGroupData.isGroup{
                let index = ArtistGroupDataManager.sharedInstance.storedArtistGroupArray.indexOf(storedData)
                //保持タブ情報から削除
                ArtistGroupDataManager.sharedInstance.storedArtistGroupArray.removeAtIndex(index!)
                //保持記事情報から開放
                let clearUrl : String = ( storedData.isGroup ?  ApiUrlEnum.ARTICLE_GROUP_URL.getUrl() : ApiUrlEnum.ARTICLE_ARTIST_URL.getUrl() ) + "/" + (storedData.id?.description)!
                ArticleDataManager.sharedInstance.clearUrlArticlesMapByUrl(clearUrl)
                ArticleDataManager.sharedInstance.clearNewTabArticlesMap()
                isMatch = true
                break;
            }
        }
        if(!isMatch){
            ArtistGroupDataManager.sharedInstance.storedArtistGroupArray.append(artistGroupData)
        }
        let nsData : NSData = NSKeyedArchiver.archivedDataWithRootObject(ArtistGroupDataManager.sharedInstance.storedArtistGroupArray)
        NSUserDefaultsManager.setObjectAndSynchronize(nsData, forKey: ArtistGroupListEnum.STORED_LIST.getNSUserDefaultKey())
    }

    override func viewWillAppear(animated: Bool) {
        if let nav = self.navigationController {
            nav.setNavigationBarHidden(false, animated: animated)
        }
        super.viewWillAppear(animated)
    }
    
    override func viewWillDisappear(animated: Bool) {
        if let nav = self.navigationController {
            nav.setNavigationBarHidden(true, animated: animated)
        }
        
        super.viewWillDisappear(animated)
    }
    
    //CustomTableViewDelegate実装
    func doReload() {
        self.artistGroupTableView.reloadData()
    }
}
