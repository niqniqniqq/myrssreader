//
//  ViewController.swift
//  MyRssReader
//
//  Created by usr0301057 on 2016/08/26.
//  Copyright © 2016年 usr0301057. All rights reserved.
//

import UIKit
import TabPageViewController

class StartViewController: UIViewController {
    
    var tabCon : TabPageViewController?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        //初回起動判定
        let userDefault : NSUserDefaults = NSUserDefaults.standardUserDefaults()
        if userDefault.boolForKey(NSUserDefaultKeyEnum.FIRST_LAUNCH.getKey()) {
            firstLaunchProc()
        }
        
        //page menu初期化
        tabCon = createTabViewController()
        
        //page menu値セット
        self.addChildViewController(tabCon!)
        self.view.addSubview(tabCon!.view)
        tabCon!.didMoveToParentViewController(self)
        
    }
    
    //アプリ初回起動時にのみ実行される、アプリ初期化処理
    func firstLaunchProc() {
        let userDefault : NSUserDefaults = NSUserDefaults.standardUserDefaults()
        //新着タブを追加
        let newTabData = ArtistGroupDataManager.sharedInstance.createNewTabData()
        ArtistGroupDataManager.sharedInstance.storedArtistGroupArray.append(newTabData)
        
        //指定タブを追加
        let enumList = [FirstLaunchAddTabEnum.GROUP_1, FirstLaunchAddTabEnum.GROUP_2, FirstLaunchAddTabEnum.GROUP_3, FirstLaunchAddTabEnum.GROUP_8]
        for tabEnum in enumList {
            let tabInfo = tabEnum.getArtistGroupData()
            let tabData = ArtistGroupData.init(inputArray: [tabInfo.id, tabInfo.id, tabInfo.name], isGroup: tabInfo.isGroup)
            tabData.isAddTab = true
            ArtistGroupDataManager.sharedInstance.storedArtistGroupArray.append(tabData)
        }
        
        //タブ情報保存
        let nsData : NSData = NSKeyedArchiver.archivedDataWithRootObject(ArtistGroupDataManager.sharedInstance.storedArtistGroupArray)
        userDefault.setObject(nsData, forKey: ArtistGroupListEnum.STORED_LIST.getNSUserDefaultKey())
        
        //タブの追加方法をアラート形式で知らせる
        let alert: UIAlertController = UIAlertController(title: "料理ジャンルの追加は設定からできます", message: "", preferredStyle:  UIAlertControllerStyle.Alert)
        let okAction: UIAlertAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler:{
            (action: UIAlertAction!) -> Void in
            // バッジ、サウンド、アラートをリモート通知対象として登録する
            //        let settings = UIUserNotificationSettings(forTypes: [.Badge, .Sound, .Alert], categories: nil)
            //        UIApplication.sharedApplication().registerForRemoteNotifications()
            //        UIApplication.sharedApplication().registerUserNotificationSettings(settings)
        })
        alert.addAction(okAction)
        presentViewController(alert, animated: true, completion: nil)
        
        //push配信設定初期化
        userDefault.setInteger(1, forKey: NSUserDefaultKeyEnum.IS_MORNING_PUSH.getKey())
        userDefault.setInteger(1, forKey: NSUserDefaultKeyEnum.IS_NOON_PUSH.getKey())
        userDefault.setInteger(1, forKey: NSUserDefaultKeyEnum.IS_EVENING_PUSH.getKey())
        userDefault.setInteger(1, forKey: NSUserDefaultKeyEnum.IS_NIGHT_PUSH.getKey())
        
        //初回起動判定オフ
        userDefault.setBool(false, forKey: NSUserDefaultKeyEnum.FIRST_LAUNCH.getKey())
        
        //userDefalutファイル書出
        userDefault.synchronize()
    }
    
    func createTabViewController() -> TabPageViewController{
        //メニュータブ作成
        let tc = TabPageViewController.create()
        for tabInfo in ArtistGroupDataManager.sharedInstance.storedArtistGroupArray{
            let articleController = self.storyboard?.instantiateViewControllerWithIdentifier("ArticleCollectionView") as! ArticleCollectionViewController
            if tabInfo.isNewTab() {
                articleController.requestUrl = ApiUrlEnum.ARTICLE_NEW_URL.getUrl() + "/" + createNewTabQuery()
            } else if tabInfo.isGroup {
                articleController.requestUrl = ApiUrlEnum.ARTICLE_GROUP_URL.getUrl() + "/" + (tabInfo.id?.description)!
            } else {
                articleController.requestUrl = ApiUrlEnum.ARTICLE_ARTIST_URL.getUrl() + "/" + (tabInfo.id?.description)!
            }
            tc.tabItems.append((articleController, tabInfo.name!))
        }

        tc.isInfinity = false
        var option = TabPageOption()
        option.tabBackgroundColor = AppColorEnum.TOOL_BAR_BACKGROUND_COLOR.getUIColor()
        option.currentColor = AppColorEnum.SELECTED_COLOR.getUIColor()
        option.addStatusBar = UIView(frame: CGRectMake(0, 0, UIScreen.mainScreen().bounds.size.width, 20))
        tc.option = option
        return tc
    }
    
    //新着タブ用クエリー作成
    func createNewTabQuery() -> String{
        var workDic = ["group" : [String](), "artist" : [String]()];
        for tabInfo in ArtistGroupDataManager.sharedInstance.storedArtistGroupArray{
            if tabInfo.isNewTab() {
                continue
            }
            if tabInfo.isGroup {
                workDic["group"]?.append((tabInfo.id?.description)!)
            } else {
                workDic["artist"]?.append((tabInfo.id?.description)!)
            }
        }
        
        var urlEncodeJson : String = ""
        do{
            let jsonData = try NSJSONSerialization.dataWithJSONObject(workDic, options: NSJSONWritingOptions(rawValue: 0))
            urlEncodeJson = (NSString(data: jsonData, encoding: NSUTF8StringEncoding)?.description)!
            urlEncodeJson = urlEncodeJson.stringByAddingPercentEncodingWithAllowedCharacters(NSCharacterSet.URLQueryAllowedCharacterSet())!
        } catch {
            print("json convert error")
        }
        
        return urlEncodeJson
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

