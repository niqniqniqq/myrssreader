//
//  CostumNavigationController.swift
//  JcaleNewsReader
//
//  Created by usr0301057 on 2016/10/12.
//  Copyright © 2016年 usr0301057. All rights reserved.
//

import UIKit

class CostumNavigationController: UINavigationController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //status bar の設定を変えすための拡張
    override func childViewControllerForStatusBarStyle() -> UIViewController? {
        return self.visibleViewController;
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
