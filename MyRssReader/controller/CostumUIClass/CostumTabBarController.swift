//
//  CostumTabBarController.swift
//  MyRssReader
//
//  Created by usr0301057 on 2016/11/01.
//  Copyright © 2016年 usr0301057. All rights reserved.
//

import UIKit

class CostumTabBarController: UITabBarController, UITabBarControllerDelegate{

    override func viewDidLoad() {
        super.viewDidLoad()
        self.delegate = self

        // Do any additional setup after loading the view.
        
        //背景色
//        UITabBar.appearance().backgroundImage = UIImage()
//        UITabBar.appearance().shadowImage = UIImage()
//        UITabBar.appearance().barTintColor = AppColorEnum.TOOL_BAR_BACKGROUND_COLOR.getUIColor()
//        self.tabBar.translucent = true
        
        //選択時文字色
        let selectedAttributes = [NSForegroundColorAttributeName: AppColorEnum.SELECTED_COLOR.getUIColor()]
        UITabBarItem.appearance().setTitleTextAttributes(selectedAttributes, forState: UIControlState.Selected)
        
        //icon color
        UITabBar.appearance().tintColor = AppColorEnum.SELECTED_COLOR.getUIColor()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tabBarController(tabBarController: UITabBarController, didSelectViewController viewController: UIViewController) {
        updateTabMenu(viewController)
    }

    func updateTabMenu(startViewController: UIViewController){
        if let startViewCon = startViewController as? StartViewController {

            //start controller から subviewを削除
            for subview in startViewCon.view.subviews {
                subview.removeFromSuperview()
            }
            
            //メニュータブ作り直し
            startViewCon.loadView()
            startViewCon.viewDidLoad()
            startViewCon.tabCon?.viewWillAppear(true)
            print("#########tab update##########")
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
