//
//  TapRangeExpansionButton.swift
//  JcaleNewsReader
//
//  Created by usr0301057 on 2016/10/17.
//  Copyright © 2016年 usr0301057. All rights reserved.
//

import UIKit

class TapRangeExpansionButton: UIButton {

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    
    var insets = UIEdgeInsetsMake(0, 0, 0, 0)

    override func pointInside(point: CGPoint, withEvent event: UIEvent?) -> Bool {
        var rect = bounds
        rect.origin.x -= insets.left
        rect.origin.y -= insets.top
        rect.size.width += insets.left + insets.right
        rect.size.height += insets.top + insets.bottom
        
        return CGRectContainsPoint(rect, point)
    }
}
