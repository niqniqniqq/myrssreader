//
//  ArticleCollectionViewController.swift
//  MyRssReader
//
//  Created by usr0301057 on 2016/10/25.
//  Copyright © 2016年 usr0301057. All rights reserved.
//

import UIKit
import SafariServices
import TabPageViewController

class ArticleCollectionViewController: UICollectionViewController, CustomTableViewDelegate {

    //request Url
    var requestUrl: String?
    //参照ページ
    var nowPage : Int = 1
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.collectionView?.delegate = self
        self.collectionView?.dataSource = self
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Register cell classes
        let nib = UINib.init(nibName: "ArticleCollectionViewCell", bundle: nil)
        self.collectionView?.registerNib(nib, forCellWithReuseIdentifier: CellIdentifierEnum.ARTICLE_COLLECTION_VIEW_CELL.getIdentifier())

        // Do any additional setup after loading the view.
        
        //NSNotificationCenter へフォアグラウンドへの変更があったことを登録する
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(ArticleCollectionViewController.viewWillEnterForeground(_:)), name: UIApplicationWillEnterForegroundNotification, object: nil)
        
        //表示位置調節
        let statusHeight = UIApplication.sharedApplication().statusBarFrame.height
        let tabBarHeight = tabBarController!.tabBar.frame.size.height
        self.collectionView!.contentInset.top = statusHeight + TabPageOption().tabHeight + 10
        self.collectionView?.contentInset.bottom = tabBarHeight + 10
        
        //リフレッシュコントローラ追加
        addRefreshControl()
    }
    
    override func viewWillAppear(animated: Bool) {
        //画面に表示される際に記事取得
        ArticleDataManager.sharedInstance.delegate = self
        if nil == ArticleDataManager.sharedInstance.urlArticlesMap[requestUrl!]{
            ArticleDataManager.sharedInstance.getArticleFromApi(requestUrl!, page: nowPage.description, isRefresh: true)
        } else {
            ArticleDataManager.sharedInstance.getArticleFromApi(requestUrl!, page: nowPage.description, isRefresh: false)
        }
        super.viewWillAppear(animated)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        let displaySize = UIScreen.mainScreen().bounds.size
        let returnSize = CGSize(width: displaySize.width - 20, height: 260)
        
        return returnSize
    }
    
    override func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if nil == ArticleDataManager.sharedInstance.urlArticlesMap[requestUrl!]{
            return 0
        }
        return ArticleDataManager.sharedInstance.urlArticlesMap[requestUrl!]!.count
    }
    
    override func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let articleData = ArticleDataManager.sharedInstance.urlArticlesMap[requestUrl!]![indexPath.row]
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier(CellIdentifierEnum.ARTICLE_COLLECTION_VIEW_CELL.getIdentifier(), forIndexPath: indexPath) as! ArticleCollectionViewCell
        cell.addOption()
        //記事ID設定
        cell.articleId = articleData.articleId?.description
        //タイトル設定
        cell.setArticleTitleWithAttributes(articleData.title!)
        //credit設定
        cell.creditLabel.text = articleData.credit
        //url設定
        cell.articleUrl = articleData.url
        //画像url設定
        cell.imgUrl = articleData.imgUrl
        //配信日時設定
        cell.pubDate = articleData.pubDate
        //お気に入りボタン設定
        if ArticleDataManager.sharedInstance.isStoredArticle(articleData.articleId!){
            cell.favouriteBtn.setImage(cell.filledStarImage, forState: UIControlState.Normal)
        } else {
            cell.favouriteBtn.setImage(cell.emptyStarImage, forState: UIControlState.Normal)
        }
        
        //cellデザイン
        cell.layer.cornerRadius = 8.0
        cell.layer.masksToBounds = true
        cell.layer.borderColor = UIColor.grayColor().CGColor
        cell.layer.borderWidth = 0.5
        
        return cell
    }
    
    //cellタップ時挙動
    override func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        //cel取得
        let cell = collectionView.cellForItemAtIndexPath(indexPath) as! ArticleCollectionViewCell
        if let url = cell.articleUrl {
            let svc = MySafariViewController(URL: NSURL(string: url)!)
            presentViewController(svc, animated: true, completion: nil);
        }
        collectionView.deselectItemAtIndexPath(indexPath, animated: true)
    }
    
    func addRefreshControl(){
        let refresh = UIRefreshControl()
        refresh.attributedTitle = NSAttributedString(string: "読込中")
        refresh.tintColor = UIColor.darkGrayColor()//インジケーター色
        refresh.addTarget(self, action: #selector(ArticleCollectionViewController.refreshArticleList(_:)), forControlEvents: UIControlEvents.ValueChanged)//呼び出しメソッド設定
        self.collectionView?.alwaysBounceVertical = true//空データのとき引けるように
        self.collectionView?.addSubview(refresh)
    }
    
    func refreshArticleList(sender:AnyObject){
        print("#######refresh######")
        sender.beginRefreshing()
        ArticleDataManager.sharedInstance.getArticleFromApi(requestUrl!, page: nowPage.description, isRefresh: true)
        sender.endRefreshing()
    }
    
    //NSNotificationCenterからフォラグランドになった通知があった際に呼ばれる
    func viewWillEnterForeground(notification: NSNotification?) {
        if (self.isViewLoaded() && (self.view.window != nil)) {
            ArticleDataManager.sharedInstance.getArticleFromApi(requestUrl!, page: nowPage.description, isRefresh: true)
        }
    }
    
    private var emptyLabel : UILabel?
    
    //CustomTableViewDelegate実装
    func doReload() {
        print("#####doReload#####")
        //既にラベルが存在している場合、一度取り除き
        if let emptyLabel = emptyLabel {
            print("remove")
            emptyLabel.removeFromSuperview()
        }
        
        //描画記事が0件の場合、記事なしラベルを貼り付ける
        if 0 == ArticleDataManager.sharedInstance.urlArticlesMap[requestUrl!]?.count {
            if nil == emptyLabel {
                emptyLabel = UILabel()
                emptyLabel?.frame = CGRectMake(0, 0, self.collectionView!.frame.size.width, self.collectionView!.frame.size.height)
                emptyLabel?.backgroundColor = UIColor.whiteColor()
                emptyLabel?.text = "新着記事がありません。"
                emptyLabel?.font = UIFont(name: "HiraKakuProN-W6", size: 16.0)
                emptyLabel?.textColor = AppColorEnum.EMPTY_TITLE_COLOR.getUIColor()
                emptyLabel?.textAlignment = NSTextAlignment.Center
            }
            self.view.addSubview(emptyLabel!)
            self.view.bringSubviewToFront(emptyLabel!)
            
        } else {
            self.collectionView!.reloadData()
        }
    }
}
