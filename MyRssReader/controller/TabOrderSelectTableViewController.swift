//
//  TabOrderSelectTableViewController.swift
//  MyRssReaderUITest
//
//  Created by usr0301057 on 2016/09/05.
//  Copyright © 2016年 usr0301057. All rights reserved.
//

import UIKit
import TabPageViewController

class TabOrderSelectTableViewController: UITableViewController {
    
    //画面読み込み時点で設定されてるタブを保持する配列
    var tmpStoreArtistGroup : [ArtistGroupData]?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.delegate = self
        self.tableView.dataSource = self
        //nav bar設定
        self.title = PageTitleEnum.TAB_SELECT_PAGE.getPageTitle()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
        
        //表示位置調節
        let statusHeight = UIApplication.sharedApplication().statusBarFrame.height
        self.tableView!.contentInset.top = statusHeight
        
        //編集モードに変更
        self.tableView.editing = true
        
        //タブ並び替え検知用に、保存済みタブ一覧を保持
        tmpStoreArtistGroup = ArtistGroupDataManager.sharedInstance.storedArtistGroupArray
        
        //タブ追加ずみデータ描画
        self.tableView.reloadData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return ArtistGroupDataManager.sharedInstance.storedArtistGroupArray.count
    }

    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        //data取得
        let artistGroupData = ArtistGroupDataManager.sharedInstance.storedArtistGroupArray[indexPath.row]
        //cell取得
        let cell = tableView.dequeueReusableCellWithIdentifier(CellIdentifierEnum.ORDER_SELECT_TAB.getIdentifier(), forIndexPath: indexPath) as? TabOrderSelectTableViewCell
        // Configure the cell...
        cell?.nameLabel.text = artistGroupData.name

        return cell!
    }

     // Override to support conditional editing of the table view.
     override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // 新着タブは編集させない
        let artistGroupData = ArtistGroupDataManager.sharedInstance.storedArtistGroupArray[indexPath.row]
        if artistGroupData.id?.description == NewTabInfoEnum.ID.rawValue{
            return false
        }
        return true
     }
    
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            let artistGroupData = ArtistGroupDataManager.sharedInstance.storedArtistGroupArray[indexPath.row]
            
            //データを配列から削除
            ArtistGroupDataManager.sharedInstance.storedArtistGroupArray.removeAtIndex(indexPath.row)
            
            //userdefaultへ保持
            let nsData : NSData = NSKeyedArchiver.archivedDataWithRootObject(ArtistGroupDataManager.sharedInstance.storedArtistGroupArray)
            NSUserDefaultsManager.setObjectAndSynchronize(nsData, forKey: ArtistGroupListEnum.STORED_LIST.getNSUserDefaultKey())
            
            //保持記事情報から開放
            let clearUrl : String = ( artistGroupData.isGroup ?  ApiUrlEnum.ARTICLE_GROUP_URL.getUrl() : ApiUrlEnum.ARTICLE_ARTIST_URL.getUrl() ) + "/" + (artistGroupData.id?.description)!
            ArticleDataManager.sharedInstance.clearUrlArticlesMapByUrl(clearUrl)
            ArticleDataManager.sharedInstance.clearNewTabArticlesMap()
            
            //再描画
            tableView.reloadData()
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }

    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {
        //移動されたデータを取得する。
        let artistGroupData = ArtistGroupDataManager.sharedInstance.storedArtistGroupArray[fromIndexPath.row]
        
        //元の位置のデータを配列から削除する。
        ArtistGroupDataManager.sharedInstance.storedArtistGroupArray.removeAtIndex(fromIndexPath.row)
        
        //移動先の位置にデータを配列に挿入する。
        ArtistGroupDataManager.sharedInstance.storedArtistGroupArray.insert(artistGroupData, atIndex:toIndexPath.row)
        
        //userdefaultへ保持
        let nsData : NSData = NSKeyedArchiver.archivedDataWithRootObject(ArtistGroupDataManager.sharedInstance.storedArtistGroupArray)
        NSUserDefaultsManager.setObjectAndSynchronize(nsData, forKey: ArtistGroupListEnum.STORED_LIST.getNSUserDefaultKey())
    }
 
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    override func viewWillAppear(animated: Bool) {
        if let nav = self.navigationController {
            nav.setNavigationBarHidden(false, animated: animated)
        }
        super.viewWillAppear(animated)
    }
    
    override func viewWillDisappear(animated: Bool) {
        if let nav = self.navigationController {
            nav.setNavigationBarHidden(true, animated: animated)
        }
        super.viewWillDisappear(animated)
    }
}
