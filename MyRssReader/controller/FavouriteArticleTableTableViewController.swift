//
//  FavouriteArticleTableTableViewController.swift
//  MyRssReaderUITest
//
//  Created by usr0301057 on 2016/09/09.
//  Copyright © 2016年 usr0301057. All rights reserved.
//

import UIKit
import SafariServices

class FavouriteArticleTableTableViewController: UITableViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.delegate = self
        self.tableView.dataSource = self
        //nav bar設定
        self.title = PageTitleEnum.FAVORITE_PGAGE.getPageTitle()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
        
        //表示位置調節
        let statusHeight = UIApplication.sharedApplication().statusBarFrame.height
        self.tableView!.contentInset.top = statusHeight
        
        //編集モード
        self.tableView.editing = true
        self.tableView.allowsSelectionDuringEditing = true
        
        //nav bar tool bar 初期設定
        if let nav = self.navigationController {
            nav.setNavigationBarHidden(true, animated: false)
        }
    }

    override func viewWillAppear(animated: Bool) {
        tableView.reloadData()
        super.viewWillAppear(animated)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return ArticleDataManager.sharedInstance.storedArticleArray.count
    }

    //cell情報返却
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let articleData = ArticleDataManager.sharedInstance.storedArticleArray[indexPath.row]
        //cell取得
        let cell = tableView.dequeueReusableCellWithIdentifier(CellIdentifierEnum.FAVOURITE_ARTICLE_TABLE_VIEW_CELL.getIdentifier(), forIndexPath: indexPath) as? FavouriteArticleTableViewCell
        //cell部品オプション追加
        cell?.addOption()
        //記事ID設定
        cell?.articleId = articleData.articleId?.description
        //タイトル設定
        cell?.setArticleTitleWithAttributes(articleData.title!)
        //credit設定
        cell?.creditLabel.text = articleData.credit
        //url設定
        cell?.articleUrl = articleData.url
        //画像url設定
        cell?.imgUrl = articleData.imgUrl

        return cell!
    }
    
    //cellタップ処理
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        //cell取得
        let cell = tableView.cellForRowAtIndexPath(indexPath) as! FavouriteArticleTableViewCell
        if let url = cell.articleUrl{
            let svc = MySafariViewController(URL: NSURL(string: url)!)
            presentViewController(svc, animated: true, completion: nil);
        }
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
    }
    
    //横スワイプでの削除モードは対応しない
    override func tableView(tableView: UITableView, editingStyleForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCellEditingStyle {
        if tableView.editing {
            return UITableViewCellEditingStyle.Delete
        } else {
            return UITableViewCellEditingStyle.None
        }
    }

    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            //データを配列から削除
            ArticleDataManager.sharedInstance.storedArticleArray.removeAtIndex(indexPath.row)
            
            //userdefaultへ保持
            let nsData : NSData = NSKeyedArchiver.archivedDataWithRootObject(ArticleDataManager.sharedInstance.storedArticleArray)
            NSUserDefaultsManager.setObjectAndSynchronize(nsData, forKey: NSUserDefaultKeyEnum.FAVOURITE_ARTICLE_LIST.getKey())
            
            //再描画
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: UITableViewRowAnimation.Fade)
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }

    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {

    }
    */

    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // 並び替えは許可しない
        return false
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
}
