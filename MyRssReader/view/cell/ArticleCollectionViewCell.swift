//
//  ArticleCollectionViewCell.swift
//  MyRssReader
//
//  Created by usr0301057 on 2016/10/25.
//  Copyright © 2016年 usr0301057. All rights reserved.
//

import UIKit
import SDWebImage

class ArticleCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var articleImageView: UIImageView!
    @IBOutlet weak var indicator: UIActivityIndicatorView!
    @IBOutlet weak var favouriteBtn: TapRangeExpansionButton!
    @IBOutlet weak var creditLabel: UILabel!
    @IBOutlet weak var articleTitle: UILabel!
    
    let filledStarImage = UIImage(named: "star-on")
    let emptyStarImage = UIImage(named: "star-off")
    var articleId : String?
    var articleUrl : String?
    var pubDate : String?
    
    //imgUrlがセットされたら呼び出される
    var imgUrl : String? {
        didSet{
            if let imgUrl = imgUrl{
                setImgToImageView(imgUrl)
            }
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func prepareForReuse() {
        articleImageView.image = nil
        favouriteBtn.setImage(emptyStarImage, forState: UIControlState.Normal)
    }
    
    //自cell部品に各種オプション値を指定する
    func addOption() {
        //favouriteBtnのタップサイズを拡張
        favouriteBtn.insets = UIEdgeInsetsMake(14.5, 13.5, 14.5, 13.5)
        //font size指定
        creditLabel.font = UIFont(name: "HiraKakuProN-W3", size: 16.0)
        creditLabel.textColor = AppColorEnum.CREDIT_TITLE_COLOR.getUIColor()
    }

    func setArticleTitleWithAttributes(title : String){
        // set textString style
        let textStyle = NSMutableParagraphStyle()
        // space between each line
        textStyle.lineSpacing = 4.0
        // set textString font style
        let textFontStyle: String = "HiraKakuProN-W6"
        let textFontSize: CGFloat = 16
        let textFont = UIFont(name: textFontStyle, size: textFontSize)!
        
        // set all style attributes
        let attributes: Dictionary = [NSParagraphStyleAttributeName: textStyle, NSFontAttributeName: textFont]
        articleTitle.attributedText = NSAttributedString(string: title, attributes: attributes)
    }
    
    func setImgToImageView(imgUrl : String){
        if let imgUrl = NSURL.init(string: imgUrl){
            SDWebImageManager.sharedManager().downloadImageWithURL(imgUrl, options: SDWebImageOptions.CacheMemoryOnly, progress: nil, completed: { ( img : UIImage!, error : NSError!, cacheType : SDImageCacheType, bool : Bool, nsUrl : NSURL!) in
                //画像リサイズ
                if nil != img {
                    self.articleImageView.image = ImageUtil.cropImageToRectangle(img, dstWidth: 380, dstHeight: 200)
                }
                UIView.animateWithDuration(0.25){
                    self.articleImageView.alpha = 1
                    self.indicator.stopAnimating();
                }
            })
            
            //角丸
//            self.articleImageView.layer.cornerRadius = 4.0;
            //アスペクト比を崩さない
            self.articleImageView.contentMode = UIViewContentMode.ScaleAspectFill
            //デザイン範囲外を描画しない
            self.articleImageView.clipsToBounds = true
        }
    }
    
    @IBAction func tapFavouriteBtn(sender: AnyObject) {
        //btn更新
        if favouriteBtn.imageView?.image == filledStarImage {
            favouriteBtn.setImage(emptyStarImage, forState: UIControlState.Normal)
            //お気に入り記事削除
            for i in 0...(ArticleDataManager.sharedInstance.storedArticleArray.count - 1){
                let articleData : ArticleData = ArticleDataManager.sharedInstance.storedArticleArray[i]
                if articleData.articleId?.description == articleId!{
                    print("#######favourite article deleted########")
                    ArticleDataManager.sharedInstance.storedArticleArray.removeAtIndex(i)
                    break
                }
            }
        } else if favouriteBtn.imageView?.image == emptyStarImage{
            favouriteBtn.setImage(filledStarImage, forState: UIControlState.Normal)
            //お気に入り記事に保存
            var isSave : Bool = true
            for articleData in ArticleDataManager.sharedInstance.storedArticleArray {
                if articleData.articleId?.description == articleId!{
                    isSave = false
                    break
                }
            }
            if isSave {
                print("#######favourite article saved########")
                let initArray : [String] = [articleId!, articleTitle.text!, articleUrl!, creditLabel.text!, pubDate!, imgUrl!]
                let articleData = ArticleData(inputArray: initArray);
                ArticleDataManager.sharedInstance.storedArticleArray.insert(articleData, atIndex: 0)
            }
        }
        //userdefaultへ保持
        let nsData : NSData = NSKeyedArchiver.archivedDataWithRootObject(ArticleDataManager.sharedInstance.storedArticleArray)
        NSUserDefaultsManager.setObjectAndSynchronize(nsData, forKey: NSUserDefaultKeyEnum.FAVOURITE_ARTICLE_LIST.getKey())
    }
}
