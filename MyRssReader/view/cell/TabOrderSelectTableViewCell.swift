//
//  TabOrderSelectTableViewCell.swift
//  MyRssReaderUITest
//
//  Created by usr0301057 on 2016/09/05.
//  Copyright © 2016年 usr0301057. All rights reserved.
//

import UIKit

class TabOrderSelectTableViewCell: UITableViewCell {

    @IBOutlet weak var nameLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
