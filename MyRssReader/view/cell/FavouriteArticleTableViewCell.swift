//
//  FavouriteArticleTableViewCell.swift
//  JcaleNewsReader
//
//  Created by usr0301057 on 2016/10/04.
//  Copyright © 2016年 usr0301057. All rights reserved.
//

import UIKit
import SDWebImage

class FavouriteArticleTableViewCell: UITableViewCell {
    
    @IBOutlet weak var articleTitle: UILabel!
    @IBOutlet weak var articleImageView: UIImageView!
    @IBOutlet weak var indicator: UIActivityIndicatorView!
    @IBOutlet weak var creditLabel: UILabel!
    
    var articleId : String?
    var articleUrl : String?
    
    //imgUrlがセットされたら呼び出される
    var imgUrl : String? {
        didSet{
            if let imgUrl = imgUrl{
                setImgToImageView(imgUrl)
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    override func prepareForReuse() {
        articleImageView.image = nil
    }
    
    //自cell部品に各種オプション値を指定する
    func addOption() {
        //font size指定
        creditLabel.font = UIFont(name: "HiraKakuProN-W3", size: 14.0)
        creditLabel.textColor = AppColorEnum.CREDIT_TITLE_COLOR.getUIColor()
    }
    
    func setArticleTitleWithAttributes(title : String){
        // set textString style
        let textStyle = NSMutableParagraphStyle()
        // space between each line
        textStyle.lineSpacing = 4.0
        // set textString font style
        let textFontStyle: String = "HiraKakuProN-W6"
        let textFontSize: CGFloat = 16
        let textFont = UIFont(name: textFontStyle, size: textFontSize)!
        
        // set all style attributes
        let attributes: Dictionary = [NSParagraphStyleAttributeName: textStyle, NSFontAttributeName: textFont]
        articleTitle.attributedText = NSAttributedString(string: title, attributes: attributes)
    }

    func setImgToImageView(imgUrl : String){
        if let imgUrl = NSURL.init(string: imgUrl){
            SDWebImageManager.sharedManager().downloadImageWithURL(imgUrl, options: SDWebImageOptions.CacheMemoryOnly, progress: nil, completed: { ( img : UIImage!, error : NSError!, cacheType : SDImageCacheType, bool : Bool, nsUrl : NSURL!) in
                //正方形に切り抜き
                if nil != img {
                    self.articleImageView.image = ImageUtil.cropImageToSquare(img)
                }
                UIView.animateWithDuration(0.25){
                    self.articleImageView.alpha = 1
                    self.indicator.stopAnimating();
                }
            })
            
            //角丸
            self.articleImageView.layer.cornerRadius = 4.0;
            //アスペクト比を崩さない
            self.articleImageView.contentMode = UIViewContentMode.ScaleAspectFit
            //デザイン範囲外を描画しない
            self.articleImageView.clipsToBounds = true
        }
    }
}
