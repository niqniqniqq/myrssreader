//
//  DateFormatEnum.swift
//  JcaleNewsReader
//
//  Created by usr0301057 on 2016/10/11.
//  Copyright © 2016年 usr0301057. All rights reserved.
//

import Foundation

enum DateFormatEnum {
    case ymdhmsFormat1
    case ymdhmsFormat2
    case ymdFormat1
    
    func getFormat() -> String{
        switch self {
        case .ymdhmsFormat1:
            return "yyyy-MM-dd HH:mm:ss"
        case .ymdhmsFormat2:
            return "yyyy/MM/dd HH:mm:ss"
        case .ymdFormat1:
            return "yyyy-MM-dd"
        }
    }
}