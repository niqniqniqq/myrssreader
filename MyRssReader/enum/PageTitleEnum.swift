//
//  PageTitleEnum.swift
//  MyRssReaderUITest
//
//  Created by usr0301057 on 2016/09/02.
//  Copyright © 2016年 usr0301057. All rights reserved.
//

import Foundation

//各ページタイトル管理用
enum PageTitleEnum {
    case MAIN_PAGE
    case ADD_TAB_PAGE
    case TAB_SELECT_PAGE
    case FAVORITE_PGAGE
    case SHARE_PGAGE
    
    func getPageTitle() -> String {
        switch self {
        case .MAIN_PAGE:
            return "ニュースリーダー"
        case .ADD_TAB_PAGE:
            return "タブの追加"
        case .TAB_SELECT_PAGE:
            return "タブの並び替えと非表示選択"
        case .FAVORITE_PGAGE:
            return "お気に入り記事"
        case .SHARE_PGAGE:
            return "シェア方法選択"
        }
    }
}
