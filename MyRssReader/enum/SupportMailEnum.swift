//
//  SupportMailEnum.swift
//  MyRssReaderUITest
//
//  Created by usr0301057 on 2016/09/09.
//  Copyright © 2016年 usr0301057. All rights reserved.
//

import Foundation

//サポートメールについてのEnum
enum SupportMailEnum : String{
    case TITLE = "についてのお問い合わせ"
    case TO_MAIL = "niqniqniqq@gmail.jp"
    case DISCRIPTION = "-----------以下は消さないで下さい-----------\n"
}
