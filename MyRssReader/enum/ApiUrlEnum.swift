//
//  ApiUrlEnum.swift
//  JcaleNewsReader
//
//  Created by usr0301057 on 2016/09/16.
//  Copyright © 2016年 usr0301057. All rights reserved.
//

import Foundation

//リクエスト先URLをまとめたenum
enum ApiUrlEnum {
    case ENDPOINT_URL
    case ARTICLE_ARTIST_URL
    case ARTICLE_GROUP_URL
    case ARTICLE_NEW_URL
    case ARTIST_LIST
    case GROUP_LIST
    case SUB_GROUP_LIST
    case REVIEW_URL
    
    func getUrl() -> String {
        switch self {
        case .ENDPOINT_URL:
            return "http://api.autorace.click/"
        case .ARTICLE_ARTIST_URL:
            return "http://api.autorace.click/xml/artist"
        case .ARTICLE_GROUP_URL:
            return "http://api.autorace.click/xml/group"
        case .ARTICLE_NEW_URL:
            return "http://api.autorace.click/xml/new"
        case .ARTIST_LIST:
            return "http://api.autorace.click/xml/artistlist"
        case .GROUP_LIST:
            return "http://api.autorace.click/xml/grouplist"
        case .SUB_GROUP_LIST:
            return "http://api.autorace.click/xml/subgrouplist"
        case .REVIEW_URL:
            return "itms-apps://itunes.apple.com/WebObjects/MZStore.woa/wa/viewContentsUserReviews?type=Purple+Software&id=1177305415"
        }
    }
}
