//
//  NewTabInfoEnum.swift
//  MyRssReaderUITest
//
//  Created by usr0301057 on 2016/09/08.
//  Copyright © 2016年 usr0301057. All rights reserved.
//

import Foundation

//新着タブ用ENUM
enum NewTabInfoEnum : String {
    case NAME = "新着"
    case ID = "0"
}