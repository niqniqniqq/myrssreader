//
//  ArtistGroupListEnum.swift
//  MyRssReaderUITest
//
//  Created by usr0301057 on 2016/09/02.
//  Copyright © 2016年 usr0301057. All rights reserved.
//

import Foundation

//アーティスト・グループ一覧の処理定数をまとめたenum
enum ArtistGroupListEnum {
    case ARTIST_LIST
    case GROUP_LIST
    case SUB_GROUP_LIST
    case STORED_LIST
    
    func getUrl() -> String {
        switch self {
        case .ARTIST_LIST:
            return ApiUrlEnum.ARTIST_LIST.getUrl()
        case .GROUP_LIST:
            return ApiUrlEnum.GROUP_LIST.getUrl()
        case .SUB_GROUP_LIST:
            return ApiUrlEnum.SUB_GROUP_LIST.getUrl()
        case .STORED_LIST:
            return ""
        }
    }
    
    func getNSUserDefaultKey() -> String {
        switch self {
        case .ARTIST_LIST:
            return NSUserDefaultKeyEnum.ARTIST_LIST.getKey()
        case .SUB_GROUP_LIST, .GROUP_LIST:
            return NSUserDefaultKeyEnum.GROUP_LIST.getKey()
        case .STORED_LIST:
            return NSUserDefaultKeyEnum.STORED_LIST.getKey()
        }
    }
}

//how to use
//println(ArtistGroupListEnum.ARTIST_LIST.getUrl())
