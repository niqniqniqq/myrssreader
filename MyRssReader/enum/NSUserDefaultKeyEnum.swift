//
//  NSUserDefaultKeyEnum.swift
//  MyRssReaderUITest
//
//  Created by usr0301057 on 2016/09/07.
//  Copyright © 2016年 usr0301057. All rights reserved.
//

import Foundation

//NSUserDefaultsに保存するキーをまとめるEnum
enum NSUserDefaultKeyEnum {
    case FIRST_LAUNCH //Bool
    case DEVICE_TOKEN //String
    case USER_ID //String
    case ARTIST_LIST //未使用
    case GROUP_LIST //未使用
    case STORED_LIST //[ArtistGroupData]
    case FAVOURITE_ARTICLE_LIST //[ArticleData]
    case IS_EVENING_PUSH //Bool
    case IS_MORNING_PUSH //BOol
    case IS_NOON_PUSH //Bool
    case IS_NIGHT_PUSH //Bool
    
    func getKey() -> String {
        switch self {
        case .FIRST_LAUNCH:
            return "firstLaunch"
        case .DEVICE_TOKEN:
            return "deviceToken"
        case .USER_ID:
            return "userID"
        case .ARTIST_LIST:
            return "artistList"
        case .GROUP_LIST:
            return "groupList"
        case .STORED_LIST:
            return "storedList"
        case .FAVOURITE_ARTICLE_LIST:
            return "favourite_article_list"
        case .IS_MORNING_PUSH:
            return "is_morning_pish"
        case .IS_NOON_PUSH:
            return "is_noon_push"
        case .IS_EVENING_PUSH:
            return "is_evening_push"
        case .IS_NIGHT_PUSH:
            return "is_night_push"
        }
    }
}
