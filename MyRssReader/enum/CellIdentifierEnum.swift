//
//  TableCellIdentifierEnum.swift
//  MyRssReaderUITest
//
//  Created by usr0301057 on 2016/09/12.
//  Copyright © 2016年 usr0301057. All rights reserved.
//

import Foundation

//Cellのidentirier管理用Enum
enum CellIdentifierEnum {
    //ArticleTableViewController
    case ARTICLE_TABLE_VIEW_CELL
    //FavouriteArticleTableViewController
    case FAVOURITE_ARTICLE_TABLE_VIEW_CELL
    //TabOrderSelectTableViewController
    case ORDER_SELECT_TAB
    //SharePageTableViewController
    case POST_TWITTER
    case SEND_LINE
    case SEND_MAIL
    //ArtistGroupSegmentViewControoler
    case ARTIST_GROUP_CELL
    //MenuTableViewController
    case FAVOURITE_ARTICLE
    case ADD_TAB
    case TAB_ORDER_SELECT
    case SUPPORT
    case WRITE_REVIEW
    case TELL_APP
    //article cocllection cell
    case ARTICLE_COLLECTION_VIEW_CELL
    
    func getIdentifier() -> String {
        switch self {
        case .ARTICLE_TABLE_VIEW_CELL:
            return "ArticleTableViewCell"
        case .FAVOURITE_ARTICLE_TABLE_VIEW_CELL:
            return "FavouriteArticleTableViewCell"
        case .ORDER_SELECT_TAB:
            return "orderSelectTab"
        case .POST_TWITTER:
            return "PostTwitter"
        case .SEND_LINE:
            return "SendLine"
        case .SEND_MAIL:
            return "SendMail"
        case .ARTIST_GROUP_CELL:
            return "ArtistGroupCell"
        case .FAVOURITE_ARTICLE:
            return "FavouriteArticle"
        case .ADD_TAB:
            return "AddTab"
        case .TAB_ORDER_SELECT:
            return "TabOrderSelect"
        case .SUPPORT:
            return "Support"
        case .WRITE_REVIEW:
            return "WriteReview"
        case .TELL_APP:
            return "TellApp"
        case .ARTICLE_COLLECTION_VIEW_CELL:
            return "ArticleCollectionCell"
        }
    }
}
