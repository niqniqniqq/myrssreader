//
//  FirstLaunchAddTabEnum.swift
//  MyRssReader
//
//  Created by usr0301057 on 2016/11/17.
//  Copyright © 2016年 usr0301057. All rights reserved.
//

import Foundation

//初期起動時 追加タブENUM
enum FirstLaunchAddTabEnum {
    case GROUP_1 //和食(1,group)
    case GROUP_2 //洋食(2,group)
    case GROUP_3 //中華・アジア(3,group)
    case GROUP_8 //まとめ(8,group)
    
    func getArtistGroupData() -> (id : String, name : String, isGroup : Bool){
        switch self {
        case .GROUP_1:
            return ("1", "和食", true)
        case .GROUP_2:
            return ("2", "洋食", true)
        case .GROUP_3:
            return ("3", "中華・アジア", true)
        case .GROUP_8:
            return ("8", "まとめ", true)
        }
    }
    
}
