//
//  SharePageEnum.swift
//  MyRssReader
//
//  Created by usr0301057 on 2016/11/01.
//  Copyright © 2016年 usr0301057. All rights reserved.
//

import Foundation

enum SharePageEnum : String {
    //mail
    case SHARE_MAIL_TITLE = "好きな料理ジャンルの最新ニュースを読もう"
    case SHARE_MAIL_DISCRIPTION = "\nhttps://goo.gl/vDTiUa"
    //line
    case SHARE_LINE_DESCRIPTION = "好みの料理ジャンルの最新ニュースを読もう\nhttps://goo.gl/vDTiUa"
    //twitter
    case SHARE_TWITTER_DESCRIPTION = "好きな料理ジャンルの最新ニュースを読もう\nhttps://goo.gl/vDTiUa"
}
