//
//  AppColorEnum.swift
//  JcaleNewsReader
//
//  Created by usr0301057 on 2016/10/12.
//  Copyright © 2016年 usr0301057. All rights reserved.
//

import Foundation
import UIKit

enum AppColorEnum {
    case SELECTED_COLOR
    case GOLD_COLOR
    case UNSELECTED_MENU_COLOR
    case INDICATOR_COLOR
    case BACK_BTN_COLOR
    case ARTICEL_TITLE_COLOR
    case CREDIT_TITLE_COLOR
    case TOOL_BAR_BACKGROUND_COLOR
        case EMPTY_TITLE_COLOR
    
    func getUIColor() -> UIColor{
        switch self {
        case .GOLD_COLOR:
            return UIColor.init(red: 174/255, green: 151/255, blue: 114/255, alpha: 1)
        case .UNSELECTED_MENU_COLOR:
            return UIColor.init(red: 210/255, green: 198/255, blue: 177/255, alpha: 1)
        case .INDICATOR_COLOR:
            return UIColor.init(red: 238/255, green: 226/255, blue: 208/255, alpha: 1)
        case .BACK_BTN_COLOR:
            return UIColor.init(red: 238/255, green: 226/255, blue: 208/255, alpha: 1)
        case .ARTICEL_TITLE_COLOR:
            return UIColor.blackColor()
        case .CREDIT_TITLE_COLOR:
            return UIColor.init(red: 141/255, green: 141/255, blue: 141/255, alpha: 1)
        case .SELECTED_COLOR:
            return UIColor(red: 246/255, green: 175/255, blue: 32/255, alpha: 1.0)
        case .TOOL_BAR_BACKGROUND_COLOR:
            return UIColor(red: 1.0, green: 1.0, blue: 1.0, alpha: 0.95)
        case .EMPTY_TITLE_COLOR:
            return UIColor.init(red: 170/255, green: 170/255, blue: 170/255, alpha: 1)
        }
    }
}
