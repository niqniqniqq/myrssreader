//
//  ArtistGroupData.swift
//  MyRssReaderUITest
//
//  Created by usr0301057 on 2016/09/05.
//  Copyright © 2016年 usr0301057. All rights reserved.
//

import Foundation

class ArtistGroupData : NSObject, NSCoding{
    
    var id : Int?
    
    var name : String?
    
    var isGroup : Bool = false
    
    var isAddTab : Bool = false
    
    var isDisp : Bool = false
    
    var tabOrder : Int?
    
    //シリアライズ用キー一覧
    let nsCodingKey : (id : String, name : String, tabOrder : String, isGroup : String, isAddTab : String, isDisp : String) = ("id", "name", "tabOrder", "isGroup", "isAddTab", "isDisp")
    
    init(inputArray : [String], isGroup : Bool){
        id = Int(inputArray[1])
        name = inputArray[2]
        self.isGroup = isGroup
    }
    
    //新着タブか判別します
    func isNewTab() -> Bool{
        var result : Bool = false;
        
        if(NewTabInfoEnum.ID.rawValue == self.id?.description && NewTabInfoEnum.NAME.rawValue == name){
            result = true;
        }
        
        return result
    }
    
    //プロトコル NSCoding
    required init?(coder aDecoder: NSCoder) {
        id = aDecoder.decodeObjectForKey(nsCodingKey.id) as? Int
        name = aDecoder.decodeObjectForKey(nsCodingKey.name) as? String
        tabOrder = aDecoder.decodeObjectForKey(nsCodingKey.tabOrder) as? Int
        isGroup = aDecoder.decodeBoolForKey(nsCodingKey.isGroup)
        isAddTab = aDecoder.decodeBoolForKey(nsCodingKey.isAddTab)
        isDisp = aDecoder.decodeBoolForKey(nsCodingKey.isDisp)
    }
    
    //プロトコル NSCoding
    func encodeWithCoder(aCoder: NSCoder) {
        aCoder.encodeObject(id, forKey: nsCodingKey.id)
        aCoder.encodeObject(name, forKey: nsCodingKey.name)
        aCoder.encodeObject(tabOrder, forKey: nsCodingKey.tabOrder)
        aCoder.encodeBool(isGroup, forKey: nsCodingKey.isGroup)
        aCoder.encodeBool(isAddTab, forKey: nsCodingKey.isAddTab)
        aCoder.encodeBool(isDisp, forKey: nsCodingKey.isDisp)
    }
}

class ArtistGroupDataManager : NSObject, NSXMLParserDelegate, Validator{
    
    //シングルトンオブジェクトの作成
    static let sharedInstance : ArtistGroupDataManager = ArtistGroupDataManager()
    //共有記憶に保存された値を格納する配列
    var storedArtistGroupArray = [ArtistGroupData]()
    //apiから取得した値を格納配列
    var artistGroupArray = [ArtistGroupData]()
    //delegate
    var delegate : CustomTableViewDelegate?
    //共有記憶
    let userDefault : NSUserDefaults = NSUserDefaults.standardUserDefaults()
    
    //インスタンス生成されないよう制御
    private override init(){
        super.init()
        storedArtistGroupArray = getStoredArtistGroupDatasFromNSUserDefaults()
    }
    
    //data取得処理
    func getArtistFromApi(targetEnum : ArtistGroupListEnum){
        //初期化
        artistGroupArray = [ArtistGroupData]()
        //validation
        if !validateURL(targetEnum.getUrl()){
            return
        }
        //urlリクエスト
        let session = NSURLSession.sharedSession()
        //一覧取得
        if let nsUrl = NSURL.init(string: targetEnum.getUrl()){
            let request = NSURLRequest.init(URL: nsUrl)
            let task = session.dataTaskWithRequest(request, completionHandler: { (nsData : NSData?, nsUrlResponse : NSURLResponse?, nsError : NSError?) in
                //エラー処理
                if let error = nsError{
                    print("######dataTaskWithRequest error#####" + error.description)
                    return
                }
                
                //200以外のステータスコード検知
                if let httpResponse = nsUrlResponse as? NSHTTPURLResponse{
                    if 200 != httpResponse.statusCode{
                        return
                    }
                }
                
                //取得xmlデータ処理
                let xmlParseObj = ArtistGroupListXmlParser(input: nsData!)
                let xmlItemArray = xmlParseObj.getArtistGroupData()
                
                //artistデータ処理
                let isGroup : Bool = ArtistGroupListEnum.ARTIST_LIST == targetEnum ? false : true;
                for itemArray in xmlItemArray{
                    let artistGroupData = ArtistGroupData(inputArray: itemArray, isGroup: isGroup)
                    self.artistGroupArray.append(artistGroupData)
                }
                
                //タブ追加処理情報等付加
                for storedArtistGroupData in self.storedArtistGroupArray{
                    for artistGroupData in self.artistGroupArray{
                        if storedArtistGroupData.id == artistGroupData.id &&
                            storedArtistGroupData.isGroup == artistGroupData.isGroup{
                            artistGroupData.isAddTab = storedArtistGroupData.isAddTab
                            artistGroupData.isDisp = storedArtistGroupData.isDisp
                            break;
                        }
                    }
                }
                
                dispatch_async(dispatch_get_main_queue(), {
                    self.delegate?.doReload()
                })
            })
            task.resume()
        }
    }
    
    // userdefaultに保存されているアーティスト or グループ情報を取得します
    func getStoredArtistGroupDatasFromNSUserDefaults() -> [ArtistGroupData]{
        let userDefault : NSUserDefaults = NSUserDefaults.standardUserDefaults()
        if let storedNSData = userDefault.objectForKey(ArtistGroupListEnum.STORED_LIST.getNSUserDefaultKey()) as? NSData{
            if let storedArtistGroupDataArray = NSKeyedUnarchiver.unarchiveObjectWithData(storedNSData) as? [ArtistGroupData]{
                self.storedArtistGroupArray = storedArtistGroupDataArray
            }
        }
        return self.storedArtistGroupArray
    }
    
    //新着タブに対応するデータを生成します
    func createNewTabData() -> ArtistGroupData{
        let input : [String] = [NewTabInfoEnum.NAME.rawValue, NewTabInfoEnum.ID.rawValue, NewTabInfoEnum.NAME.rawValue]
        return ArtistGroupData(inputArray: input, isGroup: false)
    }
    
    //引数配列とuserdefaultに保存されているアーティスト or グループ情報に違いはないか判定します
    func checkUpdate(checkArray : [ArtistGroupData]?) -> Bool {
        var isUpdate = false
        
        if nil == checkArray {
            return isUpdate
        }
        
        //保持されてるタブ数が違う場合、なんからの変更あり
        if checkArray!.count != ArtistGroupDataManager.sharedInstance.storedArtistGroupArray.count{
            isUpdate = true
        }
        
        //タブ項目を１つづつチェック
        if !isUpdate {
            for i in 0...(ArtistGroupDataManager.sharedInstance.storedArtistGroupArray.count - 1){
                let beforeDate = checkArray![i]
                let afterDate = ArtistGroupDataManager.sharedInstance.storedArtistGroupArray[i]
                if(beforeDate.id != afterDate.id){
                    isUpdate = true
                    break;
                }
                if(beforeDate.isGroup != afterDate.isGroup){
                    isUpdate = true
                    break;
                }
            }
        }
        
        return isUpdate
    }
}

