//
//  ArticleData.swift
//  MyApiTestApp
//
//  Created by usr0301057 on 2016/08/23.
//  Copyright © 2016年 usr0301057. All rights reserved.
//

import UIKit
import Foundation

class ArticleData  : NSObject, NSCoding {
    
    var articleId : Int?
    
    var title : String?
    
    var url : String?
    
    var credit : String?
    
    var pubDate : String?
    
    var imgUrl : String?
    
    //シリアライズ用キー一覧
    let nsCodingKey : (articleId : String, title : String, url : String, credit : String, pubDate : String, imgUrl : String) = ("articleId", "title", "url", "credit", "pubDate", "imgUrl")
    
    init(inputArray : [String]){
        articleId = Int(inputArray[0])
        title = inputArray[1]
        url = inputArray[2]
        credit = inputArray[3]
        pubDate = inputArray[4]
        imgUrl = inputArray[5]
    }
    
    required init?(coder aDecoder: NSCoder) {
        articleId = aDecoder.decodeObjectForKey(nsCodingKey.articleId) as? Int
        title = aDecoder.decodeObjectForKey(nsCodingKey.title) as? String
        url = aDecoder.decodeObjectForKey(nsCodingKey.url) as? String
        credit = aDecoder.decodeObjectForKey(nsCodingKey.credit) as? String
        pubDate = aDecoder.decodeObjectForKey(nsCodingKey.pubDate) as? String
        imgUrl = aDecoder.decodeObjectForKey(nsCodingKey.imgUrl) as? String
    }
    
    func encodeWithCoder(aCoder: NSCoder) {
        aCoder.encodeObject(articleId, forKey: nsCodingKey.articleId)
        aCoder.encodeObject(title, forKey: nsCodingKey.title)
        aCoder.encodeObject(url, forKey: nsCodingKey.url)
        aCoder.encodeObject(credit, forKey: nsCodingKey.credit)
        aCoder.encodeObject(pubDate, forKey: nsCodingKey.pubDate)
        aCoder.encodeObject(imgUrl, forKey: nsCodingKey.imgUrl)
    }
}

//シングルトンとして利用することを想定
//ArticleDataManager.sharedInstance.xxx
//と使うことで、データを管理するクラスをメモリ上に１つのみに制約する
class ArticleDataManager : NSObject, NSXMLParserDelegate, Validator{
    
    //1ページあたりの最大記事数
    let MAX_ARTICLE_CNT : Int = 25
    
    //シングルトンオブジェクトの作成
    static let sharedInstance : ArticleDataManager = ArticleDataManager()
    
    //url-記事情報マップ
    var urlArticlesMap = [String : [ArticleData]]()
    
    //お気に入り記事管理
    var storedArticleArray = [ArticleData]()
    
    //delegate
    var delegate : CustomTableViewDelegate?

    //インスタンス生成されないよう制御
    private override init(){
        super.init()
        storedArticleArray = getStoredArticlesFromNSUserDefaults()
    }

    //data取得処理
    func getArticleFromApi(baseUrl : String, page : String, isRefresh : Bool){
        //初期化
        if nil == urlArticlesMap[baseUrl]{
            urlArticlesMap[baseUrl] = [ArticleData]()
        }
        //リクエスト先url組み立て
        let appVersion = NSBundle.mainBundle().objectForInfoDictionaryKey("CFBundleShortVersionString") as! String
        let targetUrl : String = baseUrl + "?page=" + page + "&app_version=" + appVersion
        //validation
        if !validateURL(targetUrl){
            return
        }
        //urlリクエスト
        let session = NSURLSession.sharedSession()
        //記事取得
        if let nsUrl = NSURL.init(string: targetUrl){
            let request = NSURLRequest.init(URL: nsUrl)
            let task = session.dataTaskWithRequest(request, completionHandler: { (nsData : NSData?, nsUrlResponse : NSURLResponse?, nsError : NSError?) in
                //エラー処理
                if let error = nsError{
                    print("######dataTaskWithRequest error#####" + error.description)
                    return
                }
                
                //200以外のステータスコード検知
                if let httpResponse = nsUrlResponse as? NSHTTPURLResponse{
                    if 200 != httpResponse.statusCode{
                        print(httpResponse.statusCode)
                        return
                    }
                }
                
                //取得xmlデータ処理
                let xmlParseObj = ArticleDataXmlParser(input: nsData!)
                let xmlItemArray = xmlParseObj.getArticleData()

                //url-記事情報マップ処理
                var isReload : Bool = true
                if isRefresh{
                    //メモリー解放後全件更新
                    self.urlArticlesMap[baseUrl]?.removeAll()
                    for itemArray in xmlItemArray{
                        let articleData = ArticleData(inputArray: itemArray)
                        self.urlArticlesMap[baseUrl]?.append(articleData)
                    }
                } else {
                    //差分のみ更新
                    var isAppend : Bool = true
                    var newGetArticlels = [ArticleData]()
                    for itemArray in xmlItemArray{
                        //api取得した記事とメモリー格納済み記事の差分オブジェクト生成
                        for storedData in self.urlArticlesMap[baseUrl]!{
                            if itemArray[0] == storedData.articleId?.description{
                                isAppend = false
                                break
                            }
                        }
                        
                        if !isAppend {
                            break
                        }
                        let articleData = ArticleData(inputArray: itemArray)
                        newGetArticlels.append(articleData)
                    }
                    
                    //新規記事があった場合は、新規記事とメモリー格納済み記事を結合する
                    if 0 != newGetArticlels.count{
                        print("######append new article cnt####" + newGetArticlels.count.description)
                        for sotredData in self.urlArticlesMap[baseUrl]! {
                            if self.MAX_ARTICLE_CNT == newGetArticlels.count{
                                break
                            }
                            newGetArticlels.append(sotredData)
                        }
                        self.urlArticlesMap[baseUrl] = newGetArticlels
                    } else {
                        isReload = false
                    }
                }
                
                dispatch_async(dispatch_get_main_queue(), {
                    if isReload || 0 == self.urlArticlesMap[baseUrl]?.count {
                        self.delegate?.doReload()
                    }
                })
            })
            task.resume()
        }
    }

    /**
     userdefaultに保存されているお気に入り記事を取得します
    */
    func getStoredArticlesFromNSUserDefaults() -> [ArticleData]{
        let userDefault : NSUserDefaults = NSUserDefaults.standardUserDefaults()
        if let storedNSData = userDefault.objectForKey(NSUserDefaultKeyEnum.FAVOURITE_ARTICLE_LIST.getKey()) as? NSData{
            if let storedArticleArray = NSKeyedUnarchiver.unarchiveObjectWithData(storedNSData) as? [ArticleData]{
                self.storedArticleArray = storedArticleArray
            }
        }
        return self.storedArticleArray
    }
    
    /**
     お気に入り保存されている記事か判定します
     */
    func isStoredArticle(articleId : Int) -> Bool {
        var isMatch = false
        for articleData in storedArticleArray{
            if articleId == articleData.articleId{
                isMatch = true
                break;
            }
        }
        return isMatch
    }
    
    /**
     apiから取得した記事を開放します
     */
    func clearUrlArticlesMapByUrl(targetUrl : String){
        if nil != urlArticlesMap[targetUrl] {
            print("#####memory clear article url#####:" + targetUrl)
            urlArticlesMap.removeValueForKey(targetUrl)
        }
    }
    
    /**
     新着タブ記事を開放します
     */
    func clearNewTabArticlesMap(){
        for (key, _) in ArticleDataManager.sharedInstance.urlArticlesMap{
            if key.containsString(ApiUrlEnum.ARTICLE_NEW_URL.getUrl()){
                print("#####memory clear article url#####:" + key)
                urlArticlesMap.removeValueForKey(key)
                break
            }
        }
    }
}
