//
//  AppDelegate.swift
//  MyRssReader
//
//  Created by usr0301057 on 2016/08/26.
//  Copyright © 2016年 usr0301057. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        // Override point for customization after application launch.
        
        //初回起動時用フラグを埋め込み
        let userDefault : NSUserDefaults = NSUserDefaults.standardUserDefaults()
        let firstLaunchFlg : [String : Bool] = [NSUserDefaultKeyEnum.FIRST_LAUNCH.getKey() : true]
        userDefault.registerDefaults(firstLaunchFlg)
        
//        if (launchOptions?[UIApplicationLaunchOptionsRemoteNotificationKey]) != nil {
//            // アプリが起動していない時にpush通知が届き、push通知から起動した場合
//        }
        
        //ステータスバー全体の色を指定
        UIApplication.sharedApplication().statusBarStyle = .Default
        
        return true
    }

    func applicationWillResignActive(application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        
        //activeになったらバッジ数を0にする
//        UIApplication.sharedApplication().applicationIconBadgeNumber = 0
    }

    func applicationWillTerminate(application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }

    // Push通知の登録が完了した場合、deviceTokenが返される
    func application(application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: NSData ) {        
//        //device token から <>と" "(空白)を取る
//        let removeCharacterSet: NSCharacterSet = NSCharacterSet( charactersInString: "<>" )
//        let deviceTokenString: String = ( deviceToken.description as NSString )
//            .stringByTrimmingCharactersInSet( removeCharacterSet )
//            .stringByReplacingOccurrencesOfString( " ", withString: "" ) as String
//        //user defaultへ保存
//        print("deviceToken: \(deviceTokenString)")
//        NSUserDefaultsManager.setObjectAndSynchronize(deviceTokenString, forKey: NSUserDefaultKeyEnum.DEVICE_TOKEN.getKey())
        
    }
    
    // Push通知が利用不可であればerrorが返ってくる
    func application(application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: NSError) {
//        NSLog("error: " + "\(error)")
    }

    // Push通知受信時とPush通知をタッチして起動したときに呼ばれる
    func application(application: UIApplication, didReceiveRemoteNotification userInfo: [NSObject : AnyObject]) {
//        print(userInfo)
//        switch application.applicationState {
//        case .Inactive:
//            // アプリがバックグラウンドにいる状態で、Push通知から起動したとき
//            break
//        case .Active:
//            // アプリ起動時にPush通知を受信したとき
//            break
//        case .Background:
//            // アプリがバックグラウンドにいる状態でPush通知を受信したとき
//            //起動したらバッジ数を1加算
//            UIApplication.sharedApplication().applicationIconBadgeNumber += 1
//            break
//        }
    }
}

