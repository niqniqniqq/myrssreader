//
//  CustomTableViewDelegate.swift
//  MyRssReaderUITest
//
//  Created by usr0301057 on 2016/09/05.
//  Copyright © 2016年 usr0301057. All rights reserved.
//

import Foundation

protocol CustomTableViewDelegate {
    // デリゲートメソッド定義
    func doReload()
}